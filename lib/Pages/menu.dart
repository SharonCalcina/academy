import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

typedef void FetchMoreCallback(
  Map<String, dynamic> variables,
  MergeResults mergeResults,
);

typedef Map<String, dynamic> MergeResults(
  dynamic prev,
  dynamic moreResults,
);

typedef Widget FetchMoreBuilder(
  QueryResult result,
  FetchMoreCallback fetchMore,
);

class Menu extends StatefulWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  List nomCol = ["a", "b", "c"];
  String readSchool = """ 
  query {
    colegios{
      rows{
        id
        nombre
        direccion
        distrito
      }
    }
  }
  """;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Menu CRUD"),
        ),
        body: Container(
            child: Query(
                options: QueryOptions(
                  document: gql(readSchool),
                  /*variables: {
                    'nRepositories': 5,
                  },
                  pollInterval: Duration(seconds: 10),*/
                ),
                builder: (QueryResult result, {fetchMore, refetch}) {
                  if (result.hasException) {
                    return Text(result.exception.toString());
                  }

                  if (result.isLoading) {
                    return Text('Loading');
                  }
                  Map<String, dynamic>? res = result.data;
                  print(res);
                  return Text(res.toString());
                })));
  }

  Widget listaCole() {
    return Container(
        child: ListView.builder(
      itemBuilder: (BuildContext contex, item) {
        return ListTile(
          leading: Text(nomCol[item]),
        );
      },
      itemCount: nomCol.length,
    ));
  }
}
