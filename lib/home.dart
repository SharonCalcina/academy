import 'package:academy/Pages/menu.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
          child: Center(
              child: Stack(
        children: [
          Container(
            height: h,
            width: w,
            child: Opacity(
              opacity: 0.6,
              child: Image.asset(
                "assets/images/college.jpg",
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 200),
                width: w,
                child: text("Bienvenido", 20.0, FontWeight.bold,
                    FontStyle.italic, Colors.white),
              ),
              Container(
                  padding: EdgeInsets.only(top: 50, bottom: h / 3),
                  width: w,
                  child: text("SchoolApp", 30.0, FontWeight.bold,
                      FontStyle.italic, Colors.yellow)),
              button(
                  Menu(),
                  text("Contiunar", 15.0, FontWeight.bold, FontStyle.normal,
                      Colors.white),
                  Colors.amber.withOpacity(0.6)),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ],
      ))),
    );
  }

  Widget text(text, size, weight, style, color) {
    return Text(
      text,
      style: TextStyle(
        fontSize: size,
        fontWeight: weight,
        fontStyle: style,
        color: color,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget button(page, text, color) {
    return ElevatedButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return page;
          }));
        },
        child: text,
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(15),
            backgroundColor: color,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(50)))));
  }
}
